import telegram, yaml

with open('config.yaml', 'r') as f:
    config = yaml.load(f)

bot_token = config["bot_token"]
chatid = config["chat_id"]

bot = telegram.Bot(bot_token)

def alert(msg):
    bot.send_message(chat_id=chatid, text=msg)


