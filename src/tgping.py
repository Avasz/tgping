import time, subprocess
import yaml, datetime
import tgbot
from threading import Thread

with open('config.yaml', 'r') as f:
    config = yaml.load(f)


hosts = config["hosts"]
pingcount = str(config["pingcount"])

def pinger(host):
    p = subprocess.Popen(["ping","-q", "-c", pingcount, host], stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
    p.wait()
    status = p.poll()
    if status == 0:
        exit
    else:
        time = str(datetime.datetime.now())
        msg = "Host down: " + host + "\nTime: " + time
        tgbot.alert(msg)

for i in hosts:
    t = Thread(target=pinger, args=(i,))
    t.start()
