# About

Simple python script combined with telegram bot to ping the hosts and send a telegram
message if the host is down. It requires `python3`.

## Install python modules

This script requires two external modules:  
1. pyyaml -> for parsing the config file in YAML.  
2. python-telegram-bot -> for sending alerts to telegram group  

Install these by:

`pip3 install -r requirements.txt`

## Running

First edit the `config.yaml` file with your details.  
After that you can run it in any way you like. `alerter.py` is the main script that
needs to be run. So a simple  
`python3 alerter.py`  

You can use service files or tmux or whatever you like.  

Sample service file has been included in this repo inside `service` directory. This assumes that the `tgping` is installed in `/opt/` folder.
Change it as per your requirements. A time for the service file has also been added.

## Disclaimer

This `TG-Alert` doesn't come with any warranty. If it doesn't function as you desire, or if it didn't send a notification when your servers went down
which caused you a great loss, I will not be taking any responsibilities.   

I do however will keep on working on this and make it better as time goes by as per my needs. You are free to play, experiment or do whatever you want with it.  

Pull requests are always welcomed!! :)


## License

This software comes with WTFPL  


